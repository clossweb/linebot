'use strict';
module.exports = new botFile();

function botFile() {

    this.fs = require('fs');
    this.dirname = {
        text: './dump/text/',
        text_react: './dump/text_react/',
        watch: './dump/watch/'
    };

    this.handleFormFile = function (data_arr) {
        let result = {};
        let message;

        for (let index in data_arr) {
            try {
                message = JSON.parse(data_arr[index]);
                result[Object.keys(message)[0]] = message[Object.keys(message)[0]];
            } catch (e) {

            }
        }

        return result;
    };


    this.textOfFile = function (op) {
        let _this = this;

        if (op.data || op.uid) {
            let filenames = {};
            for (let index in this.dirname) {
                filenames[index] = this.dirname[index] + (op.uid || 'all');
            }


            if (op.data) {
                if (op.rewrite) {
                    this.fs.writeFileSync(filenames[op.type], op.data, 'utf-8');
                    return;
                }
                this.fs.appendFileSync(filenames[op.type], op.data + '\n', 'utf-8');

                return;
            }


            if (op.uid) {
                let data_arr = this.fs.readFileSync(filenames[op.type], 'utf-8').split('\n');

                return data_arr;
            }
        }

        let files = this.fs.readdirSync(_this.dirname[op.type]);
        let data = '';
        let data_arr = [];

        if (op.withUid) {
            data = {};

            for (let filename in files) {
                data[files[filename]] = this.fs.readFileSync(_this.dirname[op.type] + files[filename], 'utf-8').split('\n');
            }

            return data;
        }

        for (let filename in files) {
            data += this.fs.readFileSync(_this.dirname[op.type] + files[filename], 'utf-8');
        }

        data_arr = data.split('\n');

        return data_arr;
    };

}