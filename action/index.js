'use strict';
module.exports = function () {

    this.action = {
        help: "目前主要功能/指令：",
        cm: {
            'help': {
                FU: '_help',
                co: '幫助指令'
            },
            'me': {
                FU: 'getData',
                co: '取得自己line狀態'
            },
            'g': {
                FU: 'googleSearch',
                co: 'Google 搜尋 g [欲查詢關鍵字]'
            },
            'wa': {
                FU: 'textReactWriteDown',
                co: '設定關鍵字使BOT反應 \n wa [標題] [內容]'
            },
            'w': {
                FU: 'textWriteDown',
                co: '文字儲存(by server) \n w [標題] [內容]'
            },
            'ra': {
                FU: 'textReactRead',
                co: '讀出所設BOT反應關鍵字 \n ra [標題/無則全部]'
            },
            'r': {
                FU: 'textRead',
                co: '讀出文字(form server) \n r [標題/無則全部]'
            },
            'c': {
                FU: 'textClear',
                co: '清除文字(form server) c [標題]'
            },
            'ca': {
                FU: 'textReactClear',
                co: '清除BOT反應 ca [標題]'
            },
            'js': {
                FU: 'eval',
                co: '執行Javascript程式碼'
            },
            '現在': {
                FU: '_getTime',
                co: '取得時間 yyyy/mm/dd hh:mm'
            },
            '新聞': {
                FU: 'newsRss',
                co: '取得最新新聞(RSS) \n 新聞 [分類/無則所有] \n 新聞 聽/聽內容 [分類/無則所有]'
            },
            '笑話': {
                FU: 'joke',
                co: '笑話資源 by http://joke.876.tw/\n 笑話 [分類/無所有隨機] \n 笑話 聽 [分類/無則所有]'
            },
            'ms': {
                FU: 'mystate',
                co: 'for admin'
            }
        }
    };


    this._help = function () {
        return `${this.action.help}\n\n ${Object.keys(this.action.cm)
            .map((cm)=> {
                return `${cm}\n『${this.action.cm[cm].co}』`;
            }).join('\n---------\n')}`;
    };


//笑話
    this.joke = function (dataReadly, type) {
        let jokeType = '' || type;
        let jokeLink = '';

        let joke = require('../extra/joke');
        joke.prototype = this;
        joke = new joke();

        let _this = this;

        if (dataReadly) {

            switch (this.text_parse[this.who].ctrl) {
                case '聽':
                    this.addWatch("sayJoke", "sayJoke");
                    this.state.joke.focusType[this.who] = jokeType || null;

                    console.log(`${jokeType || 'all'} is done`);
                    this.reply(`聽對話說 ${this.state.joke.focusType[this.who] || '全部笑話'}`);

                    break;


                case '結束':
                    this.removeWatch("sayJoke", null);
                    this.state.joke.focusType[this.who] = null;
                    this.reply('笑話模式結束');

                    break;


                case '哪些':
                    _this.reply(Object.keys(this.state.joke.allType).join('\n'));
                    break;


                default :
                    let jokeTypeArr = Object.keys(_this.state.joke.data);
                    let _jokeTypeRandomIndex = Math.floor((Math.random() * jokeTypeArr.length) + 0);
                    let jokeTypeResult = jokeTypeArr[_jokeTypeRandomIndex];

                    let jokeTitleArr = Object.keys(_this.state.joke.data[jokeType || jokeTypeResult]);
                    let _jokeTitleRandomIndex = Math.floor((Math.random() * jokeTitleArr.length) + 0);

                    joke.getJokeContent(jokeType || jokeTypeResult, jokeTitleArr[_jokeTitleRandomIndex])
                        .then((Text)=>
                            this.reply(`${jokeTitleArr[_jokeTitleRandomIndex]}（${type || jokeTypeResult}）-----\n\n${Text}`));

                    break;

            }
            return;
        }

        //has a list
        if (this.state.hasOwnProperty("joke")) {

            if (this.text_parse[this.who].value) {

                for (let type in this.state.joke.allType) {
                    jokeType = (type.match(this.text_parse[this.who].value) || {input: ''}).input;

                    if (jokeType) {
                        jokeLink = this.state.joke.allType[jokeType].link;
                        break;
                    }
                }

                //has link of data
                if (this.state.joke.data.hasOwnProperty(jokeType)) {
                    this.joke(true, jokeType); //focus Type
                    return;
                }

                console.log(`getting ${jokeType} of data`);
                joke.getJokeAllLink(jokeType, this.state.joke.allType[jokeType].link)
                    .then((result)=>
                        this.joke(true, jokeType)); //focus Type

                return;
            }

            if (this.text_parse[this.who].ctrl) {
                switch (this.text_parse[this.who].ctrl) {
                    case '聽':
                        this.joke(true);
                        break;

                    case '結束':
                        this.joke(true);
                        break;

                    default :
                        this.text_parse[this.who].value = this.text_parse[this.who].ctrl;
                        this.joke();
                        break;
                }
                return;
            }


            if (Object.keys(this.state.joke.data).length == Object.keys(this.state.joke.allType).length) {

                console.log('has all');
                this.joke(true); //listen and say all joke

                return;
            }

            this.reply(`${Object.keys(this.state.joke.allType).join('\n')} \n\n聽對話說所有之笑話`);

            console.log('getting all data');

            joke.getJokeAllTypeLink()
                .then(()=> {
                    console.log('all done');

                    this.botFileCtrl.fs.writeFile(
                        `./dump/joke/${new Date().getTime()}`,
                        JSON.stringify(this.state.joke),
                        function () {
                            console.log('save all done');
                        });

                    this.joke(true); //listen and say all joke
                });


        } else {
            console.log('init');

            joke.getJokeAllType()
                .then(()=>this.joke());
        }
    };

    this.sayJoke = function () {
        if (this.text[this.who].length > 5) {
            return;
        }

        let joke = require('../extra/joke');
        joke.prototype = this;
        joke = new joke();

        let _this = this;
        let jokeTitleObj = {};
        let jokeTitle = '';
        let jokeType = this.state.joke.focusType[this.who];


        if (jokeType) {
            jokeTitleObj = this.state.joke.data[jokeType];

            if (Object.keys(jokeTitleObj).length) {
                for (let title in jokeTitleObj) {
                    jokeTitle = (title.match(this.text[this.who]) || {input: ''}).input;

                    if (jokeTitle) {
                        joke.getJokeContent(jokeType, jokeTitle)
                            .then((Text)=>
                                _this.reply(`${jokeTitle}（${jokeType}）-----\n\n${Text}`));
                        break;
                    }
                }
                return;
            }

        } else {

            for (let jokeType in this.state.joke.data) {
                jokeTitleObj = this.state.joke.data[jokeType];

                for (let title in jokeTitleObj) {
                    jokeTitle = (title.match(this.text[this.who]) || {input: ''}).input;

                    if (jokeTitle) {
                        joke.getJokeContent(jokeType, jokeTitle)
                            .then((Text)=>
                                _this.reply(`${jokeTitle}（${jokeType}）-----\n\n${Text}`));
                        return;
                    }
                }
            }
        }

    };


//新聞
    this.newsRss = function () {
        let news = require('../extra/news');
        news.prototype = this;
        news = new news();


        if (this.state.hasOwnProperty('news')) {

            let _this = this;

            let newsType;
            let newsXmlUrl;


            for (let type in this.state.news.allType) {
                newsType = (type.match(this.text_parse[this.who].value || this.text_parse[this.who].ctrl) || {input: ''}).input;

                if (newsType) {
                    newsXmlUrl = this.state.news.allType[newsType].feedUrl;
                    break;
                }
            }


            switch (this.text_parse[this.who].ctrl) {
                case '聽':

                    this.addWatch("sayNews", "sayNews");
                    this.state.news['listenContent'] = false;
                    this.state.news.focusType[this.who] = newsType || null;

                    console.log(`${newsType || 'all'} is done`);
                    this.reply(`聽對話說 ${this.state.news.focusType[this.who] || '全部新聞'}`);

                    break;

                case '聽內容':

                    this.addWatch("sayNews", "sayNews");
                    this.state.news['listenContent'] = true;
                    this.state.news.focusType[this.who] = newsType || null;

                    console.log(`${newsType || 'all'} is done`);
                    this.reply(`聽對話說 ${this.state.news.focusType[this.who] || '全部新聞'}`);

                    break;

                case '結束':
                    this.removeWatch("sayNews", null);
                    this.state.news.focusType[this.who] = null;
                    this.reply('新聞模式結束');

                    break;


                case '哪些':
                    _this.reply(Object.keys(this.state.news.allType).join('\n'));
                    break;

                default :
                    news.getNewsRssData(newsType, newsXmlUrl)
                        .then((newsArr)=> {
                            _this.reply(`[${newsType}]\n\n${newsArr.map((news)=> {
                                return `${news.title}：\n${news.link}\n${_this._getTime(news.published)}------------`;
                            }).join('\n\n')}`);

                        });
                    return;

                    break
            }

        } else { //getting title
            news.getNewsRssType()
                .then(()=>this.newsRss());
        }

    };


    this.sayNews = function (dataReadly, type) {
        if (this.text[this.who].length > 0) {
            let _this = this;
            let cheerio = require('cheerio');
            let news = require('../extra/news');
            news.prototype = this;
            news = new news();

            if (dataReadly || !news.checkoutInterval()) {

                console.log(`has ${this.state.news.focusType[this.who] || 'all'} data`);

                let field = 'title';

                if (this.state.news.listenContent) {
                    field = 'content';
                }
                let resultArr = news.getNewRssByKeyword(type, field, this.text[this.who]);

                this.reply(resultArr.map((Obj)=> {
                    return `[${Obj.type}]\n\n${
                        Obj.content.map((news)=> {
                            let $ = cheerio.load(`<html>${news.content}</html>`, {decodeEntities: false});

                            return `->${news.title}：\n\n${$('*').text().substring(0, 50)}... ${news.link}\n${_this._getTime(news.published)}------------`;
                        }).join('\n\n')}`;
                }).join('\n\n'));

                return;
            }

            news.addInterval();

            if (this.state.news.focusType[this.who]) {
                news.getNewsRssData(this.state.news.focusType[this.who], this.state.news.allType[this.state.news.focusType[this.who]].feedUrl)
                    .then(() =>this.sayNews(true, this.state.news.focusType[this.who]));
            } else {
                news.getNewsRssAllData()
                    .then(()=>this.sayNews(true));
            }

        }

    };


    this.googleSearch = function () {

        if (this.text_parse[this.who].ctrl) {

            let _this = this;

            let scraper = require('google-search-scraper');

            scraper.search({
                query: this.getValueOnly(),
                limit: this.text_parse[this.who].value || 10
            }, function (err, data) {
                // This is called for each result
                //if(err) throw err;
                _this.reply(data.map((value)=> {
                    return `${value.title}\n${value.url}`;
                }).join('\n\n'));
            });

        }
    };

    this.textReactWriteDown = function () {

        if (this.text_parse[this.who].value) {

            this.remain.text_react[this.who][this.base.event.message.id] = {
                ctrl: this.text_parse[this.who].ctrl,
                value: this.text_parse[this.who].value
            };

            let saveData = {};
            saveData[this.base.event.message.id] = this.remain.text_react[this.who][this.base.event.message.id];

            this.botFileCtrl.textOfFile({
                type: 'text_react',
                uid: this.base.event.source.userId,
                data: JSON.stringify(saveData)
            });

            return '記下反應 ' + this.text_parse[this.who].ctrl;
        }
    };

    this.textWriteDown = function () {

        if (this.text_parse[this.who].value) {

            this.remain.text[this.who][this.base.event.message.id] = {
                ctrl: this.text_parse[this.who].ctrl,
                value: this.text_parse[this.who].value
            };
            let saveData = {};
            saveData[this.base.event.message.id] = this.remain.text[this.who][this.base.event.message.id];

            this.botFileCtrl.textOfFile({
                type: 'text',
                uid: this.base.event.source.userId,
                data: JSON.stringify(saveData)
            });

            return '記下 ' + this.text_parse[this.who].ctrl;
        }
    };

    this.textRead = function () {
        let _this = this;

        let data = this.botFileCtrl.handleFormFile(
            _this.botFileCtrl.textOfFile({
                type: 'text'
            })
        );

        if (this.text_parse[this.who].ctrl) {

            for (let index in data) {
                if (data[index].ctrl.match(this.text_parse[this.who].ctrl)) {
                    return data[index].ctrl + ' : ' + data[index].value;
                }
            }

            return '找不到這筆反應';
        }
        let result = '';
        for (let index in data) {
            result += data[index].ctrl + ' : ' + data[index].value + '\n---------------------\n';
        }
        return result;
    };

    this.textReactRead = function () {
        let _this = this;

        let data = this.botFileCtrl.handleFormFile(
            _this.botFileCtrl.textOfFile({
                type: 'text_react'
            })
        );

        if (this.text_parse[this.who].ctrl) {

            for (let index in data) {
                if (data[index].ctrl.match(this.text_parse[this.who].ctrl)) {
                    return data[index].ctrl + ' : ' + data[index].value;
                }
            }

            return '找不到這筆反應';
        }
        let result = '';
        for (let index in data) {
            result += data[index].ctrl + ' : ' + data[index].value + '\n---------------------\n';
        }
        return result;
    };


    this.textClear = function () {
        let data = {};
        let dataOBJ = {};
        let key = '';

        if (this.text_parse[this.who].ctrl) {

            data = this.botFileCtrl.textOfFile({
                type: 'text',
                withUid: true
            });

            for (let filename in data) {
                for (let index in data[filename]) {
                    try {
                        dataOBJ = JSON.parse(data[filename][index]);

                        //console.log(new Buffer(JSON.stringify(dataOBJ), 'utf-8').toString('ascii'));
                        key = Object.keys(dataOBJ)[0];

                        if (dataOBJ[key].ctrl == this.text_parse[this.who].ctrl) {
                            data[filename].splice(index, 1);

                            this.botFileCtrl.textOfFile({
                                type: 'text',
                                uid: filename,
                                data: 'null\n' + data[filename].join('\n'),
                                rewrite: true
                            });
                            this.botFileCtrl.loadFormFile();
                            return dataOBJ[key].ctrl + '被清除';
                        }
                    } catch (e) {

                    }
                }
            }
            return;
        }

        return '請指定';
    };


    this.textReactClear = function () {
        let data = {};
        let dataOBJ = {};
        let key = '';

        if (this.text_parse[this.who].ctrl) {

            data = this.botFileCtrl.textOfFile({
                type: 'text_react',
                withUid: true
            });

            for (let filename in data) {
                for (let index in data[filename]) {
                    try {
                        dataOBJ = JSON.parse(data[filename][index]);

                        //console.log(new Buffer(JSON.stringify(dataOBJ), 'utf-8').toString('ascii'));
                        key = Object.keys(dataOBJ)[0];

                        if (dataOBJ[key].ctrl == this.text_parse[this.who].ctrl) {
                            data[filename].splice(index, 1);

                            this.botFileCtrl.textOfFile({
                                type: 'text_react',
                                uid: filename,
                                data: 'null\n' + data[filename].join('\n'),
                                rewrite: true
                            });
                            this.botFileCtrl.loadFormFile();
                            return dataOBJ[key].ctrl + '被清除';
                        }
                    } catch (e) {

                    }
                }
            }
            return;
        }

        return '請指定';
    };

    this.readTextResult = function () {
        let data = [];
        let result = 'UID------';

        if (this.main.text_parse[this.who].ctrl) {
            data = this.main.botFileCtrl.textOfFile({
                type: 'watch',
                uid: this.main.text_parse[this.who].ctrl
            }); //return array
        } else {
            data = this.main.botFileCtrl.textOfFile({
                type: 'watch',
                withUid: true
            }); //return object
        }
    };

    this.eval = function () {
        //if (this.text_parse[this.who].value) {
        try {
            let text = this.getValueOnly();
            let _this = this;

            return eval(text);
        } catch (e) {
            return 'javascript error';
        }
        //}
    };


    this._getTime = function (value) {
        let timeFormat = require('../extra/timeFormat.js');

        if (value) {
            return timeFormat.proceDateToStr(new Date(value), {
                which: ['Y', 'M', 'D', 'h', 'm'],
                Y: '/', M: '/', D: ' ', h: ':', m: '',
                M_z: true, D_z: true, h_z: true, m_z: true
            });
        }

        return timeFormat.proceDateToStr(new Date(), {
            which: ['Y', 'M', 'D', 'h', 'm'],
            Y: '/', M: '/', D: ' ', h: ':', m: '',
            M_z: true, D_z: true, h_z: true, m_z: true
        });
    };

    this.getData = function () {
        return JSON.stringify(this.base.event);
    };

    this.mystate = function () {
        return `who:${this.who}
        text:${this.text[this.who]}
        watching:${JSON.stringify(this.watching[this.who])}
        text_parse:${JSON.stringify(this.text_parse[this.who])}`;
    }
};