'use strict';
module.exports = new main();

function main() {

    this.base = {};
    this.who = null;
    this.text = {};
    this.text_parse = {};
    this.remain = {text: {}, text_react: {}};
    //this.watching = {};
    this.adminUID = ['Uea28e7047a09ffdcac3592d379f100b1', 'Cd751d326678f8f2d539e06a40ceab214'];
    this.watching = {focus_who: null};
    this.state = {
        fun: {
            user: {},
            texts: {
                pairList: {}
            },
            random: []
        }

    };

    this.botFileCtrl = require('../file_ctrl');


    this.fun = require('../fun');
    this.fun.prototype = this;
    this.fun = new this.fun();

    //this.act = require('../action');
    //this.act.prototype = this;
    //this.act = new this.act();

    this.init = function (line){
        this.base = {
                event: {},
                ctrl: line.client
            };

    }

    //BASE
    this.initEvent = function (event) {

        this.base.event = event;
        this.who = event.source.userId || event.source.roomId || event.source.groupId;
        this.text[this.who] = event.message.text;
        this.watching[this.who] = this.watching[this.who] || {list: [], reply: null};

        this.text_parse[this.who] = {cm: '', ctrl: '', value: ''}; //don't let it catch value last time

        let _text = event.message.text.split(' ');

        this.text_parse[this.who].cm = _text[0];
        this.text_parse[this.who].ctrl = _text[1];

        if (_text[2]) {
            _text.splice(0, 2);
            this.text_parse[this.who].value = _text.join(' ');
        }

    };

    this.main = function (event) {
        this.initEvent(event);

        let resultText;

        this.fun.init();

        if (this.fun.action.cm.hasOwnProperty(this.text_parse[this.who].cm.toLowerCase())) {
            resultText = this.fun[this.fun.action.cm[this.text_parse[this.who].cm.toLowerCase()].FU]();

        } else {
            if (this.fun.send()) {
                return;
            }
            resultText = this.fun.setDiary();
        }

        this.reply(resultText);

        return; //################################


        //if (this.act.action.cm.hasOwnProperty(this.text_parse[this.who].cm.toLowerCase())) {
        //    resultText = this.act[this.act.action.cm[this.text_parse[this.who].cm.toLowerCase()].FU]();
        //} else {
        //    resultText = (this.watching[this.who].list.map((FU)=> {
        //            return this.act[FU]();
        //        }))[this.watching[this.who].list.indexOf(this.watching[this.who].reply)] || this.textReact();
        //}
        //
        //this.push(this.who, resultText);

    };


    this.getValueOnly = function () {
        let text = this.text[this.who].split(' ');
        text.splice(0, 1);
        return text.join(' ');
    };


    this.textReact = function () {

        for (let index in this.remain.text_react[this.who]) {
            if (this.text[this.who].match(this.remain.text_react[this.who][index].ctrl)) {
                return this.remain.text_react[this.who][index].value;
            }
        }
    };

    this.loadFormFile = function () {
        let _this = this;

        this.remain.text_react[this.who] = this.botFileCtrl.handleFormFile(
            _this.botFileCtrl.textOfFile({
                type: 'text_react'
            })
        );
        this.remain.text[this.who] = this.botFileCtrl.handleFormFile(
            _this.botFileCtrl.textOfFile({
                type: 'text'
            })
        );
    };

    this.getWeb = function (url, callback, pass) {
        let request = require('request');
        let cheerio = require('cheerio');

        request(url, function (err, res, body) {
            if (res.statusCode == 200 || pass) {
                let $ = cheerio.load(body, {decodeEntities: false});
                callback(body, $, res, err);
            }
        });
    };

    this.addWatch = function (FU, replyFU) {
        let watchIndex = this.watching[this.who].list.indexOf(FU);

        this.watching[this.who].reply = null || replyFU;

        if (this.watching[this.who].list[watchIndex]) {
            return false;
        }

        this.watching[this.who].list.push(FU);
    };

    this.removeWatch = function (FU, replyFU) {
        let watchIndex = this.watching[this.who].list.indexOf(FU);

        this.watching[this.who].reply = null || replyFU;

        if (this.watching[this.who].list[watchIndex]) {
            this.watching[this.who].list.splice(watchIndex, 1);
        }
    };

    this.reply = function (content, type) {

        this.base.ctrl.replyMessage({
            replyToken: this.base.event.replyToken,
            messages: [{
                type: 'text',
                text: content
            }]
        });
    };


    this.push = function (mid, content, type) {

        if (content) {
            this.base.ctrl.pushMessage({
                to: mid,
                messages: [{
                    type: 'text',
                    text: content
                }]
            });
        }

    };


    this.getProfile = function (who) {
        return new Promise((resolve, reject)=> {
            this.base.ctrl.getProfile(who || this.who)
                .then((info)=> {
                    resolve(info.body, info);
                }, ()=> {
                    console.log('getProfile error');
                });
        });

    }
}