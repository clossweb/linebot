module.exports = new main();

function main() {

    this.base = {};

    this.init = function (event, line) {
        this.base = {
            event: event,
            ctrl: line.client
        };
    };

    this.main = function (event, line) {
        this.init(event, line);
    };


    this.push = function (content, op) {
        var message = {
            to: '{YOUR_MID}'
        };

        switch (op) {
            default:
                message['messages'] = [{
                    type: 'text',
                    text: content
                }];
                break;
        }

        this.base.ctrl.pushMessage(message);
    };

}