'use strict';
module.exports = function () {

    // Each article has the following properties:
    //
    //   * "title"     - The article title (String).
    //   * "author"    - The author's name (String).
    //   * "link"      - The original article link (String).
    //   * "content"   - The HTML content of the article (String).
    //   * "published" - The date that the article was published (Date).
    //   * "feed"      - {name, source, link}
    //

    this.feed = require('feed-read-parser');
    this.getDateIntervalMS = 180000;


    this.getNewsRssType = function () {
        let _this = this;
        const parseOpml = require('node-opml-parser');
        this.state["news"] = {
            focusType: {},
            allType: {},
            data: {},
            interval: 0
        };
        this.state["news"]["focusType"][this.who] = null;

        return new Promise((resolve)=> {
            this.getWeb('http://udn.com/udnrss/news_opml.opml', function (body) {
                parseOpml(body, (err, items) => {
                    if (err) {
                        console.error(err);
                        return;
                    }

                    resolve(items.map((Obj)=> {
                        _this.state["news"]["allType"][Obj.title] = (function () {
                            delete Obj["title"];
                            Obj.feedUrl = Obj.feedUrl.replace(/active\./, '');
                            return Obj;
                        }(Obj));

                        return Obj;
                    }));
                });

            });
        });

    }

    this.getNewsRssAllData = function (typeIndex) {
        typeIndex = typeIndex || 0;

        let type = Object.keys(this.state.news.allType)[typeIndex];


        return new Promise((resolve)=> {
            if (typeIndex > Object.keys(this.state.news.allType).length - 1) {
                resolve();
                return;
            }

            this.getNewsRssData(type, this.state.news.allType[type].feedUrl)
                .then(()=> {
                    typeIndex++;
                    this.getNewsRssAllData(typeIndex)
                        .then(()=>resolve());
                });
        });

    };


    this.getNewsRssData = function (type, xmlUrl) {
        let _this = this;
        console.log(xmlUrl);
        return new Promise((resolve, reject)=> {
            _this.feed(xmlUrl, function (err, articles) {
                try {
                    if (type) {
                        _this.state.news.data[type] = articles;
                    }

                    resolve(articles);
                } catch (e) {
                    reject(e);
                }

            });
        });
    }

    this.getNewRssByKeyword = function (type, field, keyword) {
        let data = {};
        let hasResult = [];
        let result = [];

        keyword = new RegExp(keyword, "i");

        if (type) {
            data[type] = this.state.news.data[type];
        } else {
            data = this.state.news.data;
        }


        for (let type in data) {
            hasResult = [];
            for (let index in data[type]) {
                if ((data[type][index][field].match(keyword) || {input: ''}).input) {
                    //if (data[type][index][field].search(keyword) > -1) {
                    hasResult.push(data[type][index]);
                }
            }

            if (hasResult.length) {
                result.push({type: type, content: hasResult});
            }
        }

        return result;
    }


    this.addInterval = function (setAdd) {
        let date = new Date();
        let dateTime = date.getTime();

        this.state.news.interval = dateTime + (this.getDateIntervalMS || setAdd);

    }


    this.checkoutInterval = function () {
        let date = new Date();
        let dateTime = date.getTime();

        return dateTime >= this.state.news.interval;
    }

}