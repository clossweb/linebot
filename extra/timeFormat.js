module.exports = new timeFormat();

function timeFormat (){

    this.proceDateToStr = function (date, print, hType) {

        var tempDate = new Date(date);
        var nowYear = tempDate.getFullYear();
        var nowMon = tempDate.getMonth() + 1;
        var nowDay = tempDate.getDate();
        var nowHour = tempDate.getHours();
        var nowMin = tempDate.getMinutes();
        var nowSec = tempDate.getSeconds();
        var now_Day = tempDate.getDay();
        var tempArr = [];
        var dayArr = ['��', '�@', '�G', '�T', '�|', '��', '��'];

        tempArr['Y'] = nowYear + print.Y;
        tempArr['M'] = ((nowMon < 10 && print.M_z) ? '0' + nowMon : nowMon) + print.M;
        tempArr['D'] = ((nowDay < 10 && print.D_z) ? '0' + nowDay : nowDay) + print.D;
        if (hType) {
            tempArr['h'] = ((nowHour >= 0 && nowHour < 12) ? hType.A : hType.P);
            tempArr['h'] += ((nowHour > 12) ? nowHour - 12 : nowHour) + print.h;
        } else {
            tempArr['h'] = ((nowHour < 10 && print.h_z) ? '0' + nowHour : nowHour) + print.h;
        }
        tempArr['m'] = ((nowMin < 10 && print.m_z) ? '0' + nowMin : nowMin) + print.m;
        tempArr['s'] = ((nowSec < 10 && print.s_z) ? '0' + nowSec : nowSec) + print.s;
        tempArr['d'] = ((print.d_ch) ? dayArr[now_Day] : now_Day) + print.d;

        return (tempArr[print.which[0]] || '')
            + (tempArr[print.which[1]] || '')
            + (tempArr[print.which[2]] || '')
            + (tempArr[print.which[3]] || '')
            + (tempArr[print.which[4]] || '')
            + (tempArr[print.which[5]] || '')
            + (tempArr[print.which[6]] || '');
    }

}