'use strict';
module.exports = function () {

    this.getJokeAllLink = function (type, link) {
        let _this = this;

        this.state.joke.data[type] = this.state.joke.data[type] || {};

        console.log(link);

        return new Promise((resolve)=> {
            _this.getWeb(`http://joke.876.tw/show/${link}`, function (body, $) {

                let $domChildren = {};

                for (let index = 0; index < $('.jlist').children().length; index++) {
                    $domChildren = $($('.jlist').children()[index]);
                    try {
                        if ($domChildren.children().attr('href')) {
                            _this.state.joke.data[type][$domChildren.children().attr('title')] = {
                                link: $domChildren.children().attr('href')
                            };
                        }

                    } catch (e) {

                    }
                }
                if (/list_[\d]+_([\d]+)\.shtml$/.test($('.pages a:last-child').attr('href'))) {
                    let lastPageIndex = RegExp.$1;

                    if (/list_[\d]+_([\d]+)\.shtml$/.test(link)) {
                        let currentPageIndex = RegExp.$1;
                        if (currentPageIndex == lastPageIndex) {
                            resolve();
                            return;
                        }
                        currentPageIndex = Number(currentPageIndex);
                        currentPageIndex++;
                        _this.getJokeAllLink(type, link.replace(/(list_[\d]+_)([\d]+)(\.shtml)$/, '$1' + currentPageIndex + '$3'))
                            .then(()=>
                                resolve());

                    }
                }

            });
        });

    };


    this.getJokeAllType = function () {
        let _this = this;
        this.state["joke"] = {
            allType: {},
            focusType: {},
            data: {}
        };
        this.state["joke"]["focusType"][this.who] = null;

        return new Promise((resolve)=> {
            _this.getWeb('http://joke.876.tw/show/list_12_1.shtml', function (body, $) {
                let domChildren = {};

                for (let index = 0; index < $('.jmenu').children().length; index++) {
                    domChildren = $($('.jmenu').children()[index]);
                    try {

                        if (domChildren.children().attr('href')) {

                            _this.state.joke.allType[domChildren.children().attr('title')] = {
                                link: domChildren.children().attr('href')
                            };
                        }
                    } catch (e) {

                    }

                }

                resolve();
            });
        });
    };

    this.getJokeAllTypeLink = function (typeIndex) {
        typeIndex = typeIndex || 0;

        let type = Object.keys(this.state.joke.allType)[typeIndex];


        return new Promise((resolve)=> {
            if (typeIndex > Object.keys(this.state.joke.allType).length - 1) {
                resolve();
                return;
            }

            this.getJokeAllLink(type, this.state.joke.allType[type].link)
                .then(()=> {
                    typeIndex++;
                    this.getJokeAllTypeLink(typeIndex)
                        .then(()=>resolve());
                });
        });

    };

    this.getJokeContent = function (type, title, link) {
        let _this = this;
        let getDataLink = link || this.state.joke.data[type][title].link;

        return new Promise((resolve)=> {

            if (getDataLink) {

                _this.getWeb(`http://joke.876.tw/show/${getDataLink}`, function (body, $) {
                    resolve((function () {
                        let $contentElement = $('.arts_c');
                        $contentElement.find('div,p').remove();
                        return $contentElement.html().replace(/<br>/g, '\n').trim();
                    })());
                }, true);

            }

        });

    };


};