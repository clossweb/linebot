'use strict';
const fs = require('fs');

const line = require('node-line-bot-api');
const express = require('express');
const bodyParser = require('body-parser');
const lineClient = line.client;
const lineValidator = line.validator;
const app = express();
const echo = require('./echo');


// need rawBody
app.use(bodyParser.json({
    verify (req, res, buf) {
        req.rawBody = buf
    }
}));
app.use(express.static('public'));


const config = JSON.parse(fs.readFileSync('./config.json')) || {};

// init with auth
line.init(config.line_config);

echo.init(line);
echo.loadFormFile();

app.post('/*', line.validator.validateSignature(), (req, res, next) => {

    req.body.events.map(event => {
        // reply message
        echo.main(event);
    });
});


app.listen(process.env.PORT || config.port, () => {
    console.log(config.port);
});


//log

echo.fun.adminUID.map((admin)=> { //監看有無惡意
       echo.push(admin, 'server start');
   });

// 加入 Handler  Promise REJECT LOG
// 需要 npm install bluebird 
process.on('unhandledRejection', (error) => {
   // console.error('-----------------');
   // console.error('----Promise REJECT LOG----');
   // console.error(new Date().toString());
   // console.error(error);
   // process.exit();  // 關閉NODEJS   // 擋掉後出錯就不會自動關閉SERVER
   // throw error;
   // echo.fun.adminUID.map((admin)=> { //監看有無惡意
   //     echo.push(admin, 'unhandledRejection-'+new Date().toString());
   // });
});


// // 加入 Handler  System Carsh LOG
process.on('uncaughtException', (error) => {
   console.error('-----------------');
   console.error('----uncaughtException----');
   console.error(new Date().toString());
   // process.exit();  // 關閉NODEJS   // 擋掉後出錯就不會自動關閉SERVER

   echo.fun.adminUID.map((admin)=> { //監看有無惡意
        echo.push(admin, 'uncaughtException-'+new Date().toString());
   });
   throw error;
});