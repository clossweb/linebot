'use strict';
module.exports = function () {

    this.date = null;
    this.fullDate = null;

    this.action = {
        //help: "目前主要功能/指令：",
        cm: {
            '/help': {
                FU: 'help',
                co: '反應已由line自動回應代替'
            },
            '/status': {
                FU: 'getDiary',
                co: ''
            },
            '/last': {
                FU: 'keepDiary',
                co: ''
            },
            '/reset': {
                FU: 'reset',
                co: ''
            },
            '/random': {
                FU: 'chatRandom',
                co: ''
            },
            '/chat': {
                FU: 'chatStart',
                co: ''
            },
            '/exit': {
                FU: 'chatEnd',
                co: ''
            },
            '/report': {
                FU: 'contacted',
                co: ''
            },
            '/connect': {
                FU: 'setChat',
                co: ''
            },
            '/dispair': {
                FU: 'enabledPair',
                co: ''
            },
            //縮寫
            '/p': {
                FU: 'setChat',
                co: ''
            },
            '/h': {
                FU: 'help',
                co: '反應已由line自動回應代替'
            },
            '/s': {
                FU: 'getDiary',
                co: ''
            },
            '/l': {
                FU: 'keepDiary',
                co: ''
            },
            '/n': {
                FU: 'allowRepeat',
                co: ''
            },
            '/r': {
                FU: 'chatRandom',
                co: ''
            },
            '/c': {
                FU: 'chatStart',
                co: ''
            },
            '/e': {
                FU: 'chatEnd',
                co: ''
            },
            '/a': {
                FU: 'contacted',
                co: ''
            },
            '/b': {
                FU: 'enabledPair',
                co: ''
            },
            //特殊
            '/to': {
                FU: 'setFocus',
                co: ''
            },
            '/allsay': {
                FU: 'toAll',
                co: ''
            },
            '/say': {
                FU: 'adminSay',
                co: ''
            },
            '/info': {
                FU: 'getInfo',
                co: ''
            },
            '/save': {
                FU: 'save',
                co: ''
            },
            '/load': {
                FU: 'load',
                co: ''
            },
            '/js': {
                FU: 'eval',
                co: ''
            }
        }
    };

    this.help = () => {

    }

    this.init = () => {

        this.date = new Date(new Date().getTime() + 28800000); //+8台北時區

        this.fullDate = `${this.date.getFullYear()}_${this.date.getMonth() + 1}_${this.date.getDate()}`;

        this.state.fun.user[this.who] = this.state.fun.user[this.who] || {
                focus_who: null,
                //focus_date: null,
                norepeat: {},
                state: {"myself": this.who},
                chatMode: '',
                enabled: {
                    pair: true
                }
            };

    }


    this.chatRandom = () => {
        if (this.state.fun.user[this.who].focus_who || this.state.fun.random.indexOf(this.who) > -1) {
            return '[System]聊天進行中（或等待配對中），請先退出！';
        }

        const whoRandomPair = this.state.fun.random[this.state.fun.random.length - 1];

        if (whoRandomPair) {
            if (this.checkNoRepeat(this.who, whoRandomPair)) {

                this.state.fun.user[this.who].focus_who = whoRandomPair;
                this.state.fun.user[whoRandomPair].focus_who = this.who;
                this.state.fun.random.splice(this.state.fun.random.indexOf(whoRandomPair), 1);

                this.state.fun.texts.pairList[this.who] = whoRandomPair;

                this.state.fun.user[this.who].chatMode = 'random';
                this.state.fun.user[whoRandomPair].chatMode = 'random';

                this.adminUID.map((admin)=> { //監看有無惡意
                    this.push(admin, `${this.who} and ${whoRandomPair} start chat by random`);
                });
                console.log(`${this.who} and ${whoRandomPair} start chat by random`);

                this.push(whoRandomPair, '[System]開始與人進行聊天！');
                return '[System]開始與人進行聊天！';
            }
        }

        this.state.fun.random.push(this.who);
        return '[System]等待隨機配對中！';
    }


    this.chatStart = () => {
        if (this.state.fun.user[this.who].focus_who) {
            return '[System]聊天進行中，若要更換，請先退出！';
        }

        if (this.state.fun.user[this.who].state.hasOwnProperty(this.fullDate)) {

            //配對以當日內容 若無則針對所有
            this.state.fun.user[this.who].state[this.fullDate].who =
                this.pair(this.state.fun.user[this.who].state[this.fullDate].dia) ||
                this.pair(this.state.fun.user[this.who].state[this.fullDate].dia, true);

            const whoList = this.state.fun.user[this.who].state[this.fullDate].who;
            const whoPairArr = this.checkPair(whoList).filter(isNaN);

            if (whoPairArr.length) {
                const whoPair = whoPairArr[whoPairArr.length - 1];

                this.state.fun.user[this.who].focus_who = whoPair;
                this.state.fun.user[whoPair].focus_who = this.who;

                this.state.fun.texts.pairList[this.who] = whoPair;

                this.state.fun.user[this.who].chatMode = 'today';
                this.state.fun.user[whoPair].chatMode = 'today';

                this.adminUID.map((admin)=> { //監看有無惡意
                    this.push(admin, `${this.who} and ${whoPair} start chat`);
                });
                console.log(`${this.who} and ${whoPair} start chat`);

                this.push(whoPair, '[System]開始與人進行聊天！\n目前為"允許"被動開始，若要關閉可輸入『/b n』or『/b』！\n啟用請輸入『/b y』');
                return '[System]開始與人進行聊天！';

            } else {

                return '[System]目前其他對象正進行中，請稍後再試！\n隨機馬上聊請輸入『/r』或『/random』！';

            }

        }

        return '[System]請先輸入今日心情！\n或輸入『/l』or『/last』沿用近日狀態';
    };

    this.checkPair = (whoList, allRepeat)=> {
        return whoList.map((who)=> {
            if ((this.state.fun.user[who] || {}).focus_who || this.state.fun.random.indexOf(who) > -1) {
                return false;
            }
            if ((this.checkNoRepeat(this.who, who) || allRepeat) && this.checkEnabled('pair',who)) { //該對象非使用者所禁止
                return who;
            }
            return false;
        });

    }

    this.checkNoRepeat = (myself, who, date)=> {
        date = date || this.fullDate;

        if ((this.state.fun.user[myself].norepeat[date] || []).indexOf(who) > -1 || (this.state.fun.user[who].norepeat[date] || []).indexOf(myself) > -1) {
            return false;
        }
        return true;
    }

    this.checkEnabled = (type,who) => {
        return this.state.fun.user[who].enabled[type];
    }


    this.chatEnd = () => {

        if (this.state.fun.user[this.who].focus_who) {
            const whoPair = this.state.fun.user[this.who].focus_who;
            const chatMode = this.state.fun.user[this.who].chatMode;

            //清除模式
            this.state.fun.user[this.who].chatMode = '';
            //清除對象模式
            (this.state.fun.user[whoPair] || {chatMode: ''}).chatMode = '';

            this.save('msg', true); //儲存更新整體聊天內容


            //清除與對象
            this.state.fun.user[this.who].focus_who = null;
            //回報模式結束不繼續處理
            if (chatMode == 'report') {
                return '[System]結束回報模式！';
            }
            //清除對象與我
            (this.state.fun.user[whoPair] || {focus_who: null}).focus_who = null;

            //於配對清單內清除
            if (this.state.fun.texts.pairList[this.who]) {
                delete this.state.fun.texts.pairList[this.who];
            } else {
                delete this.state.fun.texts.pairList[whoPair];
            }


            this.adminUID.map((admin)=> { //監看有無惡意
                this.push(admin, `${this.who} and ${whoPair} end chat`);
            });
            console.log(`${this.who} and ${whoPair} end chat`);


            if (this.text_parse[this.who].ctrl == 'n' || chatMode == 'today') {
                //myself
                this.state.fun.user[this.who].norepeat[this.fullDate] = this.state.fun.user[this.who].norepeat[this.fullDate] || [];
                this.state.fun.user[this.who].norepeat[this.fullDate].push(whoPair);
                //who
                this.state.fun.user[whoPair].norepeat[this.fullDate] = this.state.fun.user[whoPair].norepeat[this.fullDate] || [];
                this.state.fun.user[whoPair].norepeat[this.fullDate].push(this.who);

                this.push(whoPair, '[System]退出與該位聊天狀態（該位今日不再出現- "狀態配對預設不重複"）！');
                return '[System]退出與該位聊天狀態（該位今日不再出現- "狀態配對預設不重複"）！';
            }


            this.push(whoPair, '[System]退出與該位聊天狀態！');

            return '[System]退出與該位聊天狀態！';

        } else if (this.state.fun.random.indexOf(this.who) > -1) {

            this.state.fun.random.splice(this.state.fun.random.indexOf(this.who), 1);

            return '[System]取消等待隨機配對！';
        }

        return '[System]目前沒有對象！';
    };


    this.setDiary = ()=> {
        if (this.state.fun.user[this.who].focus_who) {
            return;
        }
        if ((this.state.fun.user[this.who].state[this.fullDate] || {}).dia) {
            return '[System]輸入『/c』或『/chat』即可配對後進行聊天，也可輸入『/r』或『/random』即進行隨機配對，\n若有問題可輸入『/h』『/help』幫助！ 輸入『/a』or『/report』可直接回報！';
        }
        if (this.text[this.who].length > 5) {

            this.state.fun.user[this.who].state[this.fullDate] = {
                who: [],
                dia: this.text[this.who]
            };

            this.adminUID.map((admin)=> { //監看有無惡意
                this.push(admin, `${this.who} -(set diary)> "${this.text[this.who]}"`);
            });
            console.log(`${this.who} set diary`);
            return '[System]今日狀態更新完成！輸入『/c』或『/chat』即可配對後進行聊天';

        }

        return '[System]輸入狀態請至少為6個字元！\n若有問題可輸入『/h』『/help』幫助！ 輸入『/a』or『/report』可直接回報！';

    }


    this.keepDiary = () => {
        if ((this.state.fun.user[this.who].state[this.fullDate] || {}).dia) {
            return '[System]今日狀態已設定！';
        }
        const self = this;
        const stateFullDateIndexLast = Object.keys(this.state.fun.user[this.who].state).length - 1;
        const stateFullDateLast = Object.keys(this.state.fun.user[this.who].state)[stateFullDateIndexLast];


        if (self.text_parse[self.who].ctrl == 's') {

            this.getProfile(this.who)
                .then((body)=> {
                    self.push(self.who, `[System]設定為 - "${body.statusMessage}"`);

                    self.state.fun.user[self.who].state[self.fullDate] = {
                        who: [],
                        dia: body.statusMessage
                    };
                }, ()=> {
                    self.push(self.who, '[System]所在聊天可能為群組，故無法取得！');
                });


        } else {
            if ((this.state.fun.user[this.who].state[stateFullDateLast] || {}).dia) {

                this.state.fun.user[this.who].state[this.fullDate] = {
                    who: [],
                    dia: this.state.fun.user[this.who].state[stateFullDateLast].dia
                };

                return `[System]延續為今日狀態 - "${this.state.fun.user[this.who].state[stateFullDateLast].dia}"`;
            }

            self.push(this.who, '[System]沒有過去紀錄！');
        }

    }


    this.enabledPair = (who,enabled) => {
        if(this.text_parse[this.who].ctrl == 'y' || enabled){
                this.state.fun.user[who || this.who].enabled.pair = true;
            return '[System]開啟允許被動配對！';
        }

        this.state.fun.user[who || this.who].enabled.pair = false;

        return '[System]關閉允許被動配對！';
    }


    this.getDiary = (who)=> {
        let result = '';
        let lastStatusData = null;

        if (this.adminUID.indexOf(this.who) > -1 && this.text_parse[this.who].ctrl) {
            who = this.text_parse[this.who].ctrl;
        }

        for (let day in this.state.fun.user[who || this.who].state) {
            lastStatusData = this.state.fun.user[who || this.who].state[day];
            if (lastStatusData.dia) {
                result += `${day}:"${lastStatusData.dia}"-指令觸發配對數(${lastStatusData.who.length})\n`;
            }
        }
        return `[System]所有每日狀況：\n${result}`;
    }


    this.pair = (text, anyDate)=> {
        text = text.replace(/[",，。\/\.\n\s\[\]{}]+/gi, '');

        const myselfWho = this.who;
        const fullDate_any = '\\d{4}_\\d{1,2}_\\d{1,2}';

        anyDate = (anyDate && fullDate_any);

        const reStr_OtherDay = `,?"[\\w]+":{"who":\[["\\w,]{0,}\],"dia":"[^"{,}]+"},?`;
        const reStr_Day = `((${reStr_OtherDay}){0,}"${anyDate || this.fullDate}":{"who":\[["\\w,]{0,}\],"dia":"[^"]{0,}[${text}]+[^"]{0,}"}(${reStr_OtherDay}){0,})+`;
        const RE = new RegExp(`("state":{"myself":"[\\w]+",${reStr_Day}})+`, 'gi');


        let result = (JSON.stringify(this.state.fun.user)
            .match(RE) || [])
            .map((str)=> {
                const who = (/"myself":"([\w]+)"/.exec(str) || [])[1];
                const dia = (new RegExp(`"${this.fullDate}":{"who":(\[["\\w,]{0,}\]),"dia":"([^"{}]+)"}`).exec(str) || [])[2];

                let countLength = 0;
                text.match(/.{1}/g)
                    .map(function (char) {
                        if (dia.search(char) > -1) {
                            countLength++;
                        }
                    });

                return {index: countLength, who: who};
            });
        result.sort((a, b)=> {
            return a.index - b.index;
        });

        return (function (whos) {
            whos.splice(whos.indexOf(myselfWho), 1); //delete myself
            return whos;
        })(
            result.map((result)=> {
                return result.who
            })
        );
    }


    this.reset = ()=> {

        switch (this.text_parse[this.who].ctrl) {
            case 'e':
                this.state.fun.user[this.who].focus_who = null;
                return '[System]取消與對象！';
                break;

            case 'w':
                if (this.adminUID.indexOf(this.who) > -1) {
                    this.watching.focus_who = null;
                    return '[System]取消監視！';
                }
                break;

            default :
                (this.state.fun.user[this.who].norepeat || {})[this.fullDate] = [];
                return '[System]允許重複！';
                break;
        }
    }


    this.setChat = function (myself, who) {
        myself = myself || this.text_parse[this.who].ctrl || this.who;
        who = who || this.text_parse[this.who].value;

        if (this.adminUID.indexOf(this.who) > -1) {
            (this.state.fun.user[myself] || {focus_who: null}).focus_who = who;
            (this.state.fun.user[who] || {focus_who: null}).focus_who = myself;

            (this.state.fun.user[myself] || {chatMode: ''}).chatMode = 'set';
            (this.state.fun.user[who] || {chatMode: ''}).chatMode = 'set';

            return `[System]設定 ${myself} 與 ${who}`;
        }
    }


    this.setFocus = ()=> {
        if (this.adminUID.indexOf(this.who) > -1) {
            if (this.text_parse[this.who].ctrl) {
                this.state.fun.user[this.who].focus_who = this.text_parse[this.who].ctrl;
                return '[System]設定回應對象為 - ' + this.text_parse[this.who].ctrl;
            }
        }
    }


    this.contacted = ()=> {
        if (this.checkPair([this.who], true)[0]) {
            this.state.fun.user[this.who].focus_who = this.adminUID[0]; //僅回報第一管理人
            this.state.fun.user[this.who].chatMode = 'report';
            //this.state.fun.user[this.who].focus_date = this.fullDate;
            return '[System]回報模式，離開為結束！';
        }
        return '[System]請先離開當前聊天，才可進行回報！';
    }


    this.toAll = ()=> {
        if (this.who == this.adminUID[0]) {
            Object.keys(this.state.fun.user).map((who)=> {
                this.send(who, `[System]${this.getValueOnly()}`, true);
            });
        }
    }


    this.adminSay = () => {
        if (this.who == this.adminUID[0]) {
            this.send(null, `[System]${this.getValueOnly()}\n-訊息來自回報對象，若要回應請輸入『/a』or『/report』後即可進行傳送！`, true);
        }
    }


    this.getInfo = ()=> {
        const self = this;
        if (this.adminUID.indexOf(this.who) > -1) {
            switch (this.text_parse[this.who].ctrl) {
                case 'user':
                    switch (this.text_parse[this.who].value) {
                        case 'list':
                            Object.keys(this.state.fun.user).map((who)=> {
                                this.getProfile(who)
                                    .then((body)=> {
                                        self.push(this.who, `${body.userId}\n暱稱：${body.displayName}\n狀態：${body.statusMessage || ''}\n大頭：${body.pictureUrl || ''}\n今天內容：${((self.state.fun.user[who].state || {})[self.fullDate] || {}).dia || ''}`);
                                    });
                            });
                            break;

                        case 'length':
                            return Object.keys(this.state.fun.user).length;
                            break;

                        default:
                            this.getProfile(this.text_parse[this.who].value)
                                .then((body)=> {
                                    self.push(this.who, `${body.userId}\n暱稱：${body.displayName}\n狀態：${body.statusMessage || ''}\n大頭：${body.pictureUrl || ''}\n今天內容：${((self.state.fun.user[self.text_parse[self.who].value].state || {})[self.fullDate] || {}).dia || ''}\n配對對象：${(this.state.fun.user[this.text_parse[this.who].value] || {}).focus_who}`);
                                });
                            break;
                    }
                    break;

                case 'pair':
                    switch (this.text_parse[this.who].value) {
                        case 'list':
                            let noRepeat = [];
                            for (let who in this.state.fun.user) {
                                if (this.state.fun.user[who].focus_who) {

                                    if (this.state.fun.user[this.state.fun.user[who].focus_who].focus_who == who && noRepeat.indexOf(who) < 0) {

                                        noRepeat.push(this.state.fun.user[who].focus_who);

                                        //this.state.fun.texts.pairList[who] = this.state.fun.user[who].focus_who;
                                        this.push(this.who, `${who} and ${this.state.fun.user[who].focus_who} pair by ${this.state.fun.user[who].chatMode}`);
                                    }
                                }
                            }

                            break;

                        case 'length':
                            this.push(this.who, Object.keys(this.state.fun.texts.pairList).length);
                            break;
                    }
                    break;

                case 'watch':
                    this.watching.focus_who = this.text_parse[this.who].value;
                    return `watching ${this.text_parse[this.who].value}`;
                    break;

                case 'status':
                    return this.getDiary(this.text_parse[this.who].value);
                    break;

                case 'sup':
                    for(let who in this.state.fun.user){
                        this.state.fun.user[who]['enabled'] = {pair:true};
                    }
                    return '[System]ok！';
                    break;

                default:
                    return;
                    break;
            }
        }
    }


    this.appendMSG = (myself, who, text) => {
        const chatMyselfIndex = (this.state.fun.user[this.state.fun.texts.pairList[myself]] || {}).focus_who || who;
        const chatWhoIndex = this.state.fun.texts.pairList[chatMyselfIndex] || who;

        this.state.fun.texts[`${chatMyselfIndex}_${chatWhoIndex}`] = this.state.fun.texts[`${chatMyselfIndex}_${chatWhoIndex}`] || [];
        this.state.fun.texts[`${chatMyselfIndex}_${chatWhoIndex}`].push({
            myself: myself,
            who: who,
            text: text,
            date: this.date.getTime()
        });

        console.log(`${myself} to ${who}`);
        //監視用
        if (this.watching.focus_who && this.watching.focus_who == chatMyselfIndex) {
            this.adminUID.map((admin)=> { //監看有無惡意
                this.push(admin, `${myself} to ${who} -(watching)> ${text}`);
            });
        }
    }


    this.send = (who, msg, adminSay) => {
        who = who || this.state.fun.user[this.who].focus_who;
        msg = (msg || this.text[this.who]);

        if (!adminSay) {
            const patt = /^\[system.{0,10}\]/i;

            if (patt.test(msg)) {
                msg = msg.replace(patt, '***'); //禁止冒充系統

                this.push(this.who, `[System]訊息包含禁用內容將替換為"${msg}"`);
            }
        }

        if(who){
            if (this.adminUID.indexOf(who) > -1) {
                this.push(who, `${this.who} -(${this.state.fun.user[this.who].chatMode})> "${this.text[this.who]}"`);
            }else{
                this.push(who, msg);
            }

            this.appendMSG(this.who, who, this.text[this.who]);

            return true;
        }

        this.appendMSG(this.who, null, this.text[this.who]);
        
        return false;
    }


    this.eval = () => {
        if (this.adminUID.indexOf(this.who) > -1) {
            try {
                let text = this.getValueOnly();
                let ziv = this;

                return eval(text);
            } catch (e) {
                return 'javascript error';
            }
        }
    };


    this.save = (op, stateVis) => {
        if (this.adminUID[0] == this.who || stateVis) {
            const self = this;
            const mode = {'msg': ['texts', this.state.fun.texts]};
            op = op || this.text_parse.ctrl;

            this.botFileCtrl.fs.writeFile(
                `./dump/namelessChat/${(mode[op] || [])[0] || this.fullDate}.json`,
                JSON.stringify((mode[op] || [])[1] || this.state.fun.user),
                (err) => {
                    if (!stateVis) {
                        if (err) {
                            console.log(err.toString());
                            self.push(self.who, '[System]儲存失敗');
                        } else {
                            console.log('save all');
                            self.push(self.who, '[System]儲存完成');
                        }
                    }
                }
            );
        }
    }


    this.load = (op) => {
        if (this.adminUID[0] == this.who) {
            const self = this;
            const mode = {'msg': ['texts']};
            op = op || this.text_parse.ctrl;

            this.botFileCtrl.fs.readFile(`./dump/namelessChat/${(mode[op] || [])[0] || this.fullDate}.json`,
                (err, dataStr) => {
                    if (err) {
                        console.log(err.toString());
                        self.push(self.who, '載入失敗');
                    } else {
                        try {
                            self.state.fun[(mode[op] || [])[0] || 'user'] = JSON.parse(dataStr);
                            console.log('read all');
                            self.push(self.who, '載入完成');
                        } catch (err) {
                            console.log(err.toString());
                            self.push(self.who, '解讀失敗');
                        }
                    }
                });
        }
    }

};

